package net.leibi.demoservice.web.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloRestController {

  @RequestMapping(
      value = "/{name}",
      produces = {"application/json", "application/problem+json"},
      method = RequestMethod.GET)
  public ResponseEntity<String> helloWithName(@PathVariable("name") String name) {
    return new ResponseEntity<>(String.format("Hello %s", name), HttpStatus.OK);
  }

  @RequestMapping(
      value = "/",
      produces = {"application/json", "application/problem+json"},
      method = RequestMethod.GET)
  public ResponseEntity<String> hello() {
    return new ResponseEntity<>(String.format("Hello there"), HttpStatus.OK);
  }
}
